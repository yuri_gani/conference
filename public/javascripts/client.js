const servers = {
    iceServers: [{
            urls: ['stun:stun01.sipphone.com']
        },
        {
            urls: ['stun:stun.ekiga.net']
        },
        {
            urls: ['stun:stun.fwdnet.net']
        },
        {
            urls: ['stun:stun.ideasip.com']
        },
        {
            urls: ['stun:stun.iptel.org']
        },
        {
            urls: ['stun:stun.rixtelecom.se']
        },
        {
            urls: ['stun:stun.schlund.de']
        },
        {
            urls: ['stun:stun.l.google.com:19302']
        },
        {
            urls: ['stun:stun1.l.google.com:19302']
        },
        {
            urls: ['stun:stun2.l.google.com:19302']
        },
        {
            urls: ['stun:stun3.l.google.com:19302']
        },
        {
            urls: ['stun:stun4.l.google.com:19302']
        },
        {
            urls: ['stun:stunserver.org']
        },
        {
            urls: ['stun:stun.softjoys.com']
        },
        {
            urls: ['stun:stun.voiparound.com']
        },
        {
            urls: ['stun:stun.voipbuster.com']
        },
        {
            urls: ['stun:stun.voipstunt.com']
        },
        {
            urls: ['stun:stun.voxgratia.org']
        },
        {
            urls: ['stun:stun.xten.com']
        },
        {
            urls: ['turn:numb.viagenie.ca'],
            credential: 'muazkh',
            username: 'webrtc@live.com'
        },
        {
            urls: [
                'turn:192.158.29.39:3478?transport=udp',
                'turn:192.158.29.39:3478?transport=tcp'
            ],
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808'
        }
    ]
};
var offerOptions = {
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 1
};
const peers = {};
var mediaConstraint = {video: true, audio: true};
window.addEventListener('load', function () {
    navigator.mediaDevices.getUserMedia(mediaConstraint)
    .then(gotLocalStream)
    .then(()=>{
        $('video#main-video').get(0).srcObject = localStream;
        wsOpen();
    })
    .catch(console.error);
});

var localStream = null;
function gotLocalStream(stream){
    new Promise((resolve, reject) => {
        localStream = stream;
        console.log(localStream);
        resolve();
    });
}

var baseWsUrl = (window.location.protocol == 'https:' ? 'wss:': 'ws:') + '//'+ window.location.host + '/';
var ws; 
var wsOpened = false;
var output;

function wsOpen() {
    console.log('opening ws', baseWsUrl);
    ws = new WebSocket(baseWsUrl);
    ws.onopen = onWsOpen.bind(ws);
    ws.onerror = onWsError.bind(ws);
    ws.onclose = onWsClose.bind(ws);
    ws.onmessage = onWsMessage.bind(ws);
}

function onWsOpen(event) {
    console.log("onOpen", event);
    wsOpened = true;
    wsSend({
        room: room,
        from: clientName,
        type: 'join'
    });
}

function onWsError(event) {
    console.log("onError", event);
}

function onWsClose(event) {
    console.log("onClose", event);
    wsOpened = false;
    wsOpen();
}

function onWsMessage(event) {
    // console.log("onWsMessage", event);
    console.log("incoming", JSON.parse(event.data));
}

function wsSend(message){
    console.log("wsSend", message);
    ws.send(typeof message === 'string' ? message: JSON.stringify(message));
}

