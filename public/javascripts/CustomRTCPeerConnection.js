class CustomRTCPeerConnection {
    constructor(servers, localName, remoteName, room) {
        this.conn = new RTCPeerConnection(servers);
        this.localName = localName;
        this.remoteName = remoteName;
        this.room = room;
        this.remoteVidEl = null;
        // this.remoteVidEl.style.display = "none";
        this.bitRate = null;
        this.mute = false;
        this.blank = false;
        this.sendFunction = (toSend) => {
            console.warn("no send function", toSend);
        };
        this.conn.onicecandidate = (e)=>{
            this.onicecandidate(e)
        }
        this.conn.ontrack = (e)=> {
            this.ontrack(e)
        }
    }

    setRemoteName(newName){
        this.remoteName = newName;
    }

    onStateChanged(fn){
        // TODO: implement listening to state changed
        this.onStateChangedCallback = fn;
    }

    setRemoteVideoEl(videoEl){
        this.remoteVidEl = videoEl;
        if (this.streams || false){
            this.ontrack({
                streams: this.streams
            })
        }
    }

    onicecandidate(event) {
        this.sendFunction({
            from: this.localName,
            room: this.room,
            to: this.remoteName,
            type: 'iceCandidate',
            payload: {
                type: event.type,
                isTrusted: event.isTrusted,
                eventPhase: event.eventPhase,
                candidate: event.candidate,
            }
        });
    }

    setBitrate(bitRate){
        // TODO: implement bitrate
    }

    setMute(mute){
        // TODO: implement muting
    }

    setSendFunction(fn){
        this.sendFunction = fn;
    }

    setBlank(blank){
        // TODO: implement blanking
    }



    doOffering(offerOptions) {
        let pc = this;
        this.offerOptions = offerOptions || this.offerOptions;
        return new Promise(function (resolve, reject) {
            pc.conn.createOffer(pc.offerOptions).then(desc=>{
                return pc.conn.setLocalDescription(desc).then(_ => {
                        pc.sendFunction({
                            from: pc.localName,
                            room: pc.room,
                            to: pc.remoteName,
                            type: 'sdp',
                            payload: desc
                        });
                        resolve(desc);
                    }, reject
                );
            })
        });
    }

    receiveOffer(desc) {
        let pc = this;
        let createNewOffer = this.localDescription == null;
        this.print("receive offering", "create new offer: "+ createNewOffer);
        return new Promise(function (resolve, reject) {
            pc.conn.setRemoteDescription(desc)
            .then(_ => pc.conn.createAnswer())
            .then(adesc => {
                return pc.conn.setLocalDescription(adesc).then(_=>{
                    pc.sendFunction({
                        from: pc.localName,
                        room: pc.room,
                        to: pc.remoteName,
                        type: 'sdp',
                        payload: adesc
                    });
                    if (createNewOffer && false){
                        return pc.doOffering(pc.offerOptions).then(resolve);
                    }else{
                        resolve();
                    }
                });
            }).catch((error)=>{
              reject(error);
            });
        });
    }

    receiveAnswer(adesc){
        return this.conn.setRemoteDescription(adesc);
    }

    addTrack(stream, streams){
        this.conn.addTrack(stream, streams);
    }
    addIceCandidate(candidate){
        if (candidate){
            console.log("adding candidate", candidate);
            this.conn.addIceCandidate(candidate).then(()=>{
                this.print("success add Ice candidate");
            }).catch(console.error);
        }
    }

    ontrack(e) {
        this.print("ontrack", e);
        if (this.remoteVidEl){
            console.log(this.localName, "ontrack");
            this.remoteVidEl.srcObject = e.streams[0];
            this.remoteVidEl.autoplay = true;
            this.remoteVidEl.playsinline = true;
            this.remoteVidEl.style.display = "block";
        }
        this.streams = e.streams;
    }
    printError() {
        console.error.apply(null, [this.localName, ...arguments]);
    }
    print() {
        console.log.apply(null, [this.localName, ...arguments]);
    }

    __noSuchMethod__(id, args){
        console.error("no such method", id, args);
    }
}
if (typeof module !== 'undefined'){
    module.exports = CustomRTCPeerConnection;
}
