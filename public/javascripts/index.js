const servers = {
    iceServers: [{
            urls: ['stun:stun01.sipphone.com']
        },
        {
            urls: ['stun:stun.ekiga.net']
        },
        {
            urls: ['stun:stun.fwdnet.net']
        },
        {
            urls: ['stun:stun.ideasip.com']
        },
        {
            urls: ['stun:stun.iptel.org']
        },
        {
            urls: ['stun:stun.rixtelecom.se']
        },
        {
            urls: ['stun:stun.schlund.de']
        },
        {
            urls: ['stun:stun.l.google.com:19302']
        },
        {
            urls: ['stun:stun1.l.google.com:19302']
        },
        {
            urls: ['stun:stun2.l.google.com:19302']
        },
        {
            urls: ['stun:stun3.l.google.com:19302']
        },
        {
            urls: ['stun:stun4.l.google.com:19302']
        },
        {
            urls: ['stun:stunserver.org']
        },
        {
            urls: ['stun:stun.softjoys.com']
        },
        {
            urls: ['stun:stun.voiparound.com']
        },
        {
            urls: ['stun:stun.voipbuster.com']
        },
        {
            urls: ['stun:stun.voipstunt.com']
        },
        {
            urls: ['stun:stun.voxgratia.org']
        },
        {
            urls: ['stun:stun.xten.com']
        },
        {
            urls: ['turn:numb.viagenie.ca'],
            credential: 'muazkh',
            username: 'webrtc@live.com'
        },
        {
            urls: [
                'turn:192.158.29.39:3478?transport=udp',
                'turn:192.158.29.39:3478?transport=tcp'
            ],
            credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            username: '28224511:1379330808'
        }
    ]
};
var offerOptions = {
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 1
};
const peers = {};
var mediaConstraint = {video: true, audio: true};
window.addEventListener('load', function () {
    navigator.mediaDevices.getUserMedia(mediaConstraint)
    .then(gotLocalStream)
    .catch(console.error);
    $('#create-peer-btn').click(function(){
        createNewConnection(Date.now())
    });
});

var localStream = null;
function gotLocalStream(stream){
    new Promise((resolve, reject) => {
        localStream = stream;
        console.log(localStream);
        resolve();
    });
}

function createNewConnection(name) {
    name = name.toString();
    peers[name] = {};
    var rvc = $('#remote-vid-container');
    var rvp = rvc.find("#peer-"+name);
    if (rvp.length == 0){ // append
        console.log("appending "+ name +" to containers");
        rvp = $(`<div class="row" id="peer-${name}"></div>`);
        rvp.appendTo(rvc);
        $(`<div class="col-xs-12 col-md-4 col-lg-3"><video id="local-${name}" autoplay muted style="display:block; width:100%;"></video></div>`).appendTo(rvp);
        rvp.find(`video#local-${name}`).get(0).srcObject = localStream;
    }
    for (const remoteName in peers) {
        if (peers.hasOwnProperty(remoteName) && remoteName != name) {
            createPeer(name, remoteName);   
            peers[name][remoteName].doOffering(offerOptions).then((sdp)=>{
                // peers[name][remoteName].print(name, remoteName, sdp);
            }).catch(error => peers[name][remoteName].printError(error));
        }
    }
}

function createPeer(localName, remoteName){
    var rvp = $("#peer-"+localName);
    console.log("create peer", localName, remoteName);
    peers[localName][remoteName] = new CustomRTCPeerConnection(servers, localName, remoteName, "bla");
    peers[localName][remoteName].setSendFunction(peerSend);
    // peers[localName][remoteName].onicecandidate = function (e){
    //     console.log("ice candidate", e);
    // }
    localStream.getTracks().forEach(track => {
        peers[localName][remoteName].addTrack(track, localStream);
    });
    
    // peers[localName][remoteName].ontrack = getRemoteStream.bind(peers[localName][remoteName]);
    $(`<div class="col-xs-12 col-md-4 col-lg-3"><video id="remote-${localName}-${remoteName}" autoplay style="display:block;width:100%;"></video></div>`).appendTo(rvp);
    peers[localName][remoteName].setRemoteVideoEl($(`video#remote-${localName}-${remoteName}`).get(0));
}

function peerSend(msg) {
    console.log("toSend", msg);
    switch(msg.type){
        case "sdp": 
            if (typeof peers[msg.to][msg.from] === 'undefined'){
                createPeer(msg.to, msg.from);
            }
            var peer = peers[msg.to][msg.from];
            if (msg.payload.type == 'offer'){
                peer.receiveOffer(msg.payload)
                // .then(adesc => peer.print(adesc))
                .then(adesc => {})
                .catch(error => peer.printError(error));
            }else{ // answer
                peer.receiveAnswer(msg.payload).then(_=>{
                    peer.print("receiving answer");
                }).catch(error => peer.printError(error));
            }
        break;
        case "iceCandidate":
            console.log("iceCandidate", msg);
            if (typeof peers[msg.to][msg.from] === 'undefined'){
                createPeer(msg.to, msg.from);
            }
            var peer = peers[msg.to][msg.from];
            peer.addIceCandidate(msg.payload.candidate);
        break;
        case "room":
        break;
        case "error":
        break;
    }
}