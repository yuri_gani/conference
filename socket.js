

module.exports = function(server){
    var uuid = require('uuid');
    var app = require('./app.js');
    var WebSocket = require('ws');

    var wss = new WebSocket.Server({
        server: server
    });
    clients = {};
    users = {};
    rooms = {};
    wss.on('connection', function(ws, rawReq){

        ws.id = uuid.v4();
        clients[ws.id] = ws;
        console.log(ws.id);
        ws.on('pong', heartbeat);
        ws.on('message', function(data){
            try {
                let msgObj = JSON.parse(data);
                if (typeof msgObj === 'object'){
                    if (msgObj.type == 'join'){
                        console.log('type == join');
                        if (msgObj.from && msgObj.room){
                            console.log('has room & from');
                            // handle from
                            this.from = msgObj.from;
                            if (typeof users[msgObj.from] === 'undefined'){
                                users[msgObj.from] = [];
                            }
                            if (users[msgObj.from].indexOf(this.id) == -1){
                                users[msgObj.from].push(this.id);
                            }

                            // handle room
                            this.room = msgObj.room;
                            if (typeof rooms[msgObj.room] === 'undefined'){
                                rooms[msgObj.room] = {
                                    members: []
                                };
                            }
                            if (rooms[msgObj.room].members.indexOf(this.id) == -1){
                                rooms[msgObj.room].members.push(this.id);
                            }

                            // handle response
                            console.log('sending members');
                            this.send(JSON.stringify({
                                type: 'members',
                                members: rooms[msgObj.room].members.map((memberId)=>{
                                    return clients[memberId].from;
                                })
                            }));
                            
                        }
                    } else if (msgObj.type == 'sdp' || msgObj.type == 'iceCandidate'){

                    } 
                }
                
            } catch (error) {
                console.error(error);   
            }
        });

        ws.on('close', cleanUp)


    });
    var heartbeat = function () {
        console.log('receive pong', this.id);
        this.isAlive = true;
    }

    var cleanUp = function(code, reason){
        console.log("clientId", this.id)
        console.log("from", this.from)
        console.log("room", this.room)
        rooms[this.room].members.splice(rooms[this.room].members.indexOf(this.room), 1);
        
    }
}