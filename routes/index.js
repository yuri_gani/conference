var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('pages/index', { 
    title: 'Express',
    css: [
      '/stylesheets/style.css'
    ],
    js: [
      '/javascripts/CustomRTCPeerConnection.js',
      '/javascripts/index.js',
    ]
  });
});

router.get('/client/:room', function(req, res, next) {
  res.render('pages/client', { 
    title: 'Express',
    room: req.params.room,
    clientId: req.session.id,
    css: [
      '/stylesheets/client.css'
    ],
    js: [
      '/javascripts/CustomRTCPeerConnection.js',
      '/javascripts/client.js',
    ]
  });
});

module.exports = router;
